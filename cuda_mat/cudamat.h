#ifndef CUDAMAT_H
#define CUDAMAT_H

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <cublas_v2.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <memory>
#include <stdexcept>

enum memType {host,dev};

template <class T>
class CudaMat
{
public:
    CudaMat();
    /*!
     * \brief This constructor fully initializes the CudaMat on either the host or device
     * \param rows number of rows in the matrix
     * \param cols number of cols in the matrix
     * \param type the type of memory you want to initalize (see the memType enum)
     */
    CudaMat(size_t rows,size_t cols, memType type = host);

    ///--------------
    /// memory management
    /// -------------

    /*!
     * \brief Set the matrix size to the specified rows and cols
     * \note does not initalize any data vectors. you must call initDev or initHost first
     * \param rows number of rows in the matrix
     * \param cols rumber of cols in the matrix
     */
    void reset( size_t rows,size_t cols);
    /*!
     * \brief initialize an empty matrix in device memory according to the set rows and cols
     */
    void initDev();
    /*!
     * \brief initialize an empty matrix in hoset memory according to the set rows and cols
     */
    void initHost();
    /*!
     * \brief syncronize the Device data to host memory
     */
    void dev2host();
    /*!
     * \brief syncronize the host memory to the device
     */
    void host2dev();

    ///--------------
    /// dimensionality
    /// -------------

    size_t rows(){ return *_rows; }
    size_t cols(){ return *_cols; }
    size_t ld(){return rows(); }
    size_t size(){return rows()*cols(); }

    ///--------------
    /// accessors
    /// -------------

    /*!
     * \brief access the memory at the specified location in HOST memory
     * \param i the ROW index
     * \param j the Column index
     * \return a reference to the requested data on the HOST
     */
    T & val(size_t i,size_t j);
    /*!
     * \brief redefine the () to wrap val(size_t i,size_t j)
     * \param i
     * \param j
     * \return a reference to the requested data on the HOST
     */
    T & operator ()(size_t i, size_t j);
    /*!
     * \brief returns a raw pointer to an array of data in DEVICE memory for compatibilty with cuda kernals and cuda API
     * \return a pointer to a basic c array of the data in DEVICE memory
     */
    T * getRawDevPtr();

    ///--------------
    /// debugging
    /// -------------

    /*!
     * \brief print out data in host memory
     */
    void printHost();
    /*!
     * \brief print out data in device memory
     * \note WARNING: thic can be expensive because it will need to syncronze host and dev memory
     */
    void printDev();

    ///--------------
    /// operators
    /// -------------


    CudaMat<T> operator *(CudaMat<T> other);


protected:
    std::shared_ptr<size_t> _rows;
    std::shared_ptr<size_t> _cols;
    std::shared_ptr<thrust::device_vector<T> > _dDat;
    std::shared_ptr<thrust::host_vector<T>   > _hDat;
};

//
// cublas core operations
//

template <class T>
CudaMat<T> mult(CudaMat<T> a, CudaMat<T> b ,
             cublasOperation_t opA = CUBLAS_OP_N, cublasOperation_t opB = CUBLAS_OP_N,
             float alpha = 1.0f, float beta = 0.0f);
/*!
 * \brief computes: y=y+alpha*x
 * \param y input/output: the cudaMat you want to add (alpha * x) to
 * \param x input only:  the cudaMat to add to y
 * \param alpha a constant scalar to multiply to x
 * \return
 */
template <class T>
void add(CudaMat<T> y, CudaMat<T> x, float alpha=1, int incy = 1, int incx = 1);



//
// Templates for calling cuda and cublas
//

#define cudacall(call)                                                                                                          \
    do                                                                                                                          \
    {                                                                                                                           \
        cudaError_t err = (call);                                                                                               \
        if(cudaSuccess != err)                                                                                                  \
        {                                                                                                                       \
            fprintf(stderr,"CUDA Error:\nFile = %s\nLine = %d\nReason = %s\n", __FILE__, __LINE__, cudaGetErrorString(err));    \
            cudaDeviceReset();                                                                                                  \
            exit(EXIT_FAILURE);                                                                                                 \
        }                                                                                                                       \
    }                                                                                                                           \
    while (0)

#define cublascall(call)                                                                                        \
    do                                                                                                          \
    {                                                                                                           \
        cublasStatus_t status = (call);                                                                         \
        if(CUBLAS_STATUS_SUCCESS != status)                                                                     \
        {                                                                                                       \
            fprintf(stderr,"CUBLAS Error:\nFile = %s\nLine = %d\nCode = %d\n", __FILE__, __LINE__, status);     \
            cudaDeviceReset();                                                                                  \
            exit(EXIT_FAILURE);                                                                                 \
        }                                                                                                       \
                                                                                                                \
    }                                                                                                           \
    while(0)


#endif // CUDAMAT_H
