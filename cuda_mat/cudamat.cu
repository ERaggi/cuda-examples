#include "cudamat.h"

template <class T>
CudaMat<T>::CudaMat()
{

}

template <class T>
CudaMat<T>::CudaMat(size_t rows,size_t cols, memType type)
{
    reset(rows,cols);
    if(type==host)
        initHost();
    if(type==dev)
        initDev();
}

template <class T>
void CudaMat<T>::reset(size_t rows,size_t cols){
    _dDat.reset(new thrust::device_vector<T>);
    _hDat.reset(new thrust::host_vector<T>  );
    _rows.reset(new size_t);
    _cols.reset(new size_t);
    *_rows=rows;
    *_cols=cols;
}

template <class T>
void CudaMat<T>::initDev(){
    _dDat->resize(this->size());
}

template <class T>
void CudaMat<T>::initHost(){
    _hDat->resize(this->size());
}

template <class T>
void CudaMat<T>::dev2host(){
    if(this->size()==_dDat->size())
        *_hDat=*_dDat;
    else{
        throw std::out_of_range("Size missmatch! device data size != CudaMat size.  Has device data been initialized or syncronized yet?");
    }

}

template <class T>
void CudaMat<T>::host2dev(){
    if(this->size()==_hDat->size())
        *_dDat=*_hDat;
    else{
        throw std::out_of_range("Size missmatch! host data size != CudaMat size.  has host data been initialized or syncronized yet?");
    }
}

template <class T>
T & CudaMat<T>::val(size_t i, size_t j){
    if(this->size()==_hDat->size())
        return _hDat->operator[](i*cols()+j);
    else{
        throw std::out_of_range("Size missmatch! host data size != CudaMat size.  has host data been initialized or syncronized yet?");
    }
}

template <class T>
T & CudaMat<T>::operator ()(size_t i, size_t j){
    return val(i,j);
}

template <class T>
T * CudaMat<T>::getRawDevPtr(){
    if(this->size()!=_dDat->size())
        throw std::out_of_range("Size missmatch! device data size != CudaMat size.  Has device data been initialized or syncronized yet?");
    return thrust::raw_pointer_cast(_dDat->data());
}

template <class T>
void CudaMat<T>::printHost(){
    for(size_t j=0; j < cols() ; j++){
        for(size_t i = 0; i < rows() ; i++){

            printf ("%3f ", val(i,j));
        }
        std::cout<< std::endl;
    }
    std::cout<< std::endl;
}

template <class T>

CudaMat<T> CudaMat<T>::operator *(CudaMat<T> other){
    return mult(*this,other);
}

//
//  BLAS operations
//
template <class T>
CudaMat<T> mult(CudaMat<T> a, CudaMat<T> b ,
             cublasOperation_t opA, cublasOperation_t opB,
             float alpha, float beta){

    cublasHandle_t handle;
    cublascall(cublasCreate_v2(&handle));

    int m = 0;
    if(opA==CUBLAS_OP_N)
        m=a.rows();
    else
        m=a.cols();

    int n = 0;
    if(opB==CUBLAS_OP_N)
        n=b.cols();
    else
        n=b.rows();

    int k = 0;
    if(opA==CUBLAS_OP_N)
        k=a.cols();
    else
        k=a.rows();


    CudaMat<T> c(m,n,dev);
    cublascall(cublasSgemm(handle,opA,opB,
                           m, n, k,
                           &alpha, a.getRawDevPtr(), a.ld(),
                                   b.getRawDevPtr(), b.ld(),
                           &beta,  c.getRawDevPtr(), c.ld()));

    cublasDestroy_v2(handle);
    return c;
}

template <class T>
void add(CudaMat<T> y, CudaMat<T> x, float alpha, int incy, int incx){
    cublasHandle_t handle;
    cublascall(cublasCreate_v2(&handle));
    cublascall(cublasSaxpy(handle,y.size(),&alpha,x.getRawDevPtr(),incx,y.getRawDevPtr(),incy));
    cublasDestroy_v2(handle);
}

int main(){
    CudaMat<float> x1(2,2);
    CudaMat<float> x2(2,2);
    CudaMat<float> x3(2,2);
    CudaMat<float> result;
    x1(0,0)=1;
    x1(1,0)=2;
    x1(0,1)=3;
    x1(1,1)=4;
    x1.printHost();
    x1.host2dev();

    x2(0,0)=5;
    x2(1,0)=6;
    x2(0,1)=7;
    x2(1,1)=8;
    x2.printHost();
    x2.host2dev();

    x3(0,0)=9;
    x3(1,0)=10;
    x3(0,1)=11;
    x3(1,1)=12;
    x3.printHost();
    x3.host2dev();

    result = x1*x2*x3;//mult(x1,x2);
    result.dev2host();
    result.printHost();

    add(x1,x2);
    x1.dev2host();
    x1.printHost();
    return 0;
}
