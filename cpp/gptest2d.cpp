#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <math.h>
#include <vector>
#include <cstdlib>

using Eigen::MatrixXd;
using namespace std;

struct point
{
    point(double x_,double y_) {x=x_;y=y_;}
    double x;
    double y;
};
/*!
 * \brief compute the covariance between two vector using the exponential kerel
 * \param x1
 * \param x2
 * \return
 */
MatrixXd expKernel(std::vector<point> x1, std::vector<point> x2){
    MatrixXd sigma(x1.size(),x2.size());
    for(size_t i = 0; i<x1.size(); i++){
        for(size_t j = 0; j<x2.size(); j++){

            sigma(i,j) = exp( (pow(x1[i].x-x2[j].x , 2) + pow(x1[i].y-x2[j].y , 2))/(2 * 100) );
            //sigma(i,j) = exp(-1*(abs(x1(i)-x2(j))));

        }
    }

    return sigma;
}

/*!
 * \brief gpSolve
 * \param xTrain
 * \param yTrain
 * \param xPred
 * \return
 */
MatrixXd gpSolve(std::vector<point> xTrain,
                 Eigen::RowVectorXd yTrain,
                 std::vector<point> xPred,
                 double sigma2e=0 ){

    MatrixXd kx_x = expKernel(xTrain,xTrain);
    MatrixXd kx_p = expKernel(xTrain,xPred);
    MatrixXd kp_x = expKernel(xPred,xTrain);
    MatrixXd kp_p = expKernel(xPred,xPred);

    MatrixXd vInv = kx_x.inverse();
    MatrixXd mu = kp_x * vInv * yTrain.transpose();
    MatrixXd var = kp_p - kp_x * vInv * kx_p;

    for(size_t i = 0 ; i< mu.size() ; i++){
        std::cout << "{" << xPred[i].x << ',' << xPred[i].y << ',' << mu(i) << "}"<<',';
    }
    return mu;
}

int main(){
    std::vector<point> x,pred;
    for(size_t i = 0 ; i<100 ; i++)
        x.push_back(point(std::rand()/((RAND_MAX)/10.0),std::rand()/((RAND_MAX)/10.0)));
    Eigen::RowVectorXd y(x.size());

    for(size_t i = 0 ; i< x.size() ; i++){
       y(i)= sin( ( x[i].x+x[i].y )/10);
    }


//    for(size_t i = 0 ; i< x.size() ; i++){
//        std::cout << "{" << x[i].x << ',' << x[i].y << ',' << y(i) << "}"<<',';
//    }


    for(float i = 0; i<10; i=i+.5){
        for(float j = 0; j<10; j=j+.5){
            pred.push_back(point(i,j));
        }
    }

    gpSolve(x,y,pred);


    return 0;
}
