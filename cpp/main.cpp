#include <iostream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <math.h>
#include <vector>
#include <chrono>

using Eigen::MatrixXd;
using namespace std;

/*!
 * \brief compute the covariance between two vector using the exponential kerel
 * \param x1
 * \param x2
 * \return
 */
MatrixXd expKernel(Eigen::RowVectorXd x1, Eigen::RowVectorXd x2){
    MatrixXd sigma(x1.size(),x2.size());
    for(size_t i = 0; i<x1.size(); i++){
        for(size_t j = 0; j<x2.size(); j++){

            //sigma(i,j) = exp(-0.5*pow(x1(i)-x2(j),2));
            sigma(i,j) = exp(-1*(abs(x1(i)-x2(j))));

        }
    }

    return sigma;
}

/*
 * \brief gpSolve
 * \param xTrain
 * \param yTrain
 * \param xPred
 * \return
 */
MatrixXd gpSolve(Eigen::RowVectorXd xTrain,
                 Eigen::RowVectorXd yTrain,
                 Eigen::RowVectorXd xPred,
                 double sigma2e=0 ){

    MatrixXd kx_x = expKernel(xTrain,xTrain);
    MatrixXd kx_p = expKernel(xTrain,xPred);
    MatrixXd kp_x = expKernel(xPred,xTrain);
    MatrixXd kp_p = expKernel(xPred,xPred);

    MatrixXd vInv = kx_x.inverse();
//    std::cout <<"vinv: " <<std::endl<< vInv << std::endl;

//    std::cout << "kp_x: " <<std::endl<< kp_x << std::endl;
    MatrixXd temp= vInv * yTrain.transpose();
    std::cout << "temp: " <<std::endl<< temp << std::endl;
    std::cout << "kp_x: " <<std::endl<< kp_x << std::endl;
    MatrixXd mu = kp_x * temp;
    MatrixXd var = kp_p - kp_x * vInv * kx_p;

    std::cout << "mu: " <<std::endl<< mu << std::endl;

    std::cout << "var: " <<std::endl << var << std::endl;
//    std::cout <<"mu: "<<  std::endl;
//    for(size_t i = 0 ; i< mu.size() ; i++){
//        std::cout <<  mu(i) << ',';
//    }

//    for(size_t i = 0 ; i< mu.size() ; i++){
//        std::cout <<  sqrt(abs(var(i)))*3 << ',';
//    }
    std::cout << std::endl;
    return mu;
}

int main(){
//{
//    auto start = std::chrono::high_resolution_clock::now();
//    Eigen::RowVectorXd x(5000),y(5000);

//    MatrixXd kx_x = expKernel(x,y);

//    auto finish = std::chrono::high_resolution_clock::now();
//    std::chrono::duration<double> elapsed = finish - start;
//    std::cout << "Elapsed time: " << elapsed.count() << " s\n";

    Eigen::RowVectorXd x(8),y,pred;
    x << 1,1.4,2.4,3,4.8,6, 8.1,9;
//    for(size_t i = 0 ; i<1000 ; i++)
//        x(i)=std::rand()/((RAND_MAX)/10.0);
    y = x;
    pred = Eigen::VectorXd::LinSpaced(11,0.0,10.0);

    for(size_t i = 0; i<x.size(); i++){
        y(i)=sin( x(i) );
    }
//    std::cout << x << std::endl;
//    std::cout << y << std::endl;

    auto start = std::chrono::high_resolution_clock::now();
    gpSolve(x,y,pred);
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Elapsed time: " << elapsed.count() << " s\n";


    return 0;
}
