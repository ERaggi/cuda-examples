
#include <stdio.h>
#define SIZE	1024


__global__
void VectorAdd(float *a, float *b, float *c, int n)
{
    int i = threadIdx.x;

    if (i < n)
        c[i] = a[i] + b[i];
}

int main()
{
    float *a, *b, *c;

    cudaMallocManaged(&a, SIZE * sizeof(float));
    cudaMallocManaged(&b, SIZE * sizeof(float));
    cudaMallocManaged(&c, SIZE * sizeof(float));

    for (int i = 0; i < SIZE; ++i)
    {
        a[i] = i;
        b[i] = i;
        c[i] = 0;
    }

    VectorAdd <<<1,SIZE>>> (a, b, c, SIZE);

    cudaDeviceSynchronize();

    for (int i = 0; i < 10; ++i)
        printf("c[%d] = %d\n", i, c[i]);

    cudaFree(a);
    cudaFree(b);
    cudaFree(c);

    return 0;
}
