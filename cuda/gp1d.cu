#undef _GLIBCXX_ATOMIC_BUILTINS
#undef _GLIBCXX_USE_INT128

#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <cublas_v2.h>
#include <iostream>
#include <math.h>
#include <vector>
#include <memory>

#define cudacall(call)                                                                                                          \
    do                                                                                                                          \
    {                                                                                                                           \
        cudaError_t err = (call);                                                                                               \
        if(cudaSuccess != err)                                                                                                  \
        {                                                                                                                       \
            fprintf(stderr,"CUDA Error:\nFile = %s\nLine = %d\nReason = %s\n", __FILE__, __LINE__, cudaGetErrorString(err));    \
            cudaDeviceReset();                                                                                                  \
            exit(EXIT_FAILURE);                                                                                                 \
        }                                                                                                                       \
    }                                                                                                                           \
    while (0)

#define cublascall(call)                                                                                        \
    do                                                                                                          \
    {                                                                                                           \
        cublasStatus_t status = (call);                                                                         \
        if(CUBLAS_STATUS_SUCCESS != status)                                                                     \
        {                                                                                                       \
            fprintf(stderr,"CUBLAS Error:\nFile = %s\nLine = %d\nCode = %d\n", __FILE__, __LINE__, status);     \
            cudaDeviceReset();                                                                                  \
            exit(EXIT_FAILURE);                                                                                 \
        }                                                                                                       \
                                                                                                                \
    }                                                                                                           \
    while(0)


// data structures

/*!
 * \brief The cudaMat class
 */
class cudaMat{
public:
    cudaMat(){return;}
    void reset(size_t rows_,size_t cols_){
        rows=rows_;
        cols=cols_;
    }
    void initDev(){
        dDat.resize(rows*cols);
    }
    void initHost(){
        hDat.resize(rows*cols);
    }

    float & val(size_t i,size_t j){
        return hDat[i*cols+j];
    }

    size_t ld(){
        return rows;
    }

    /*!
     * \brief prints the data currently on the device
     */
    void print(){
        hDat=dDat;
        for(size_t j=0; j < cols ; j++){
            for(size_t i = 0; i < rows ; i++){

                printf ("%3f ", hDat[i*cols+j]);
            }
            std::cout<< std::endl;
        }
    }

    /*!
     * \brief operator [] returns the specified element on the HOST vector
     * \param i
     * \param j
     */
    float operator ()(size_t i, size_t j){
        return hDat[i*cols+j];
    }

    void dev2host(){
        hDat=dDat;
    }
    void host2dev(){
        dDat=hDat;
    }

    size_t size(){
        return rows*cols;
    }

    /*!
     * \brief this function is used for compatibility with cuda kernals and libraries.  It produces a standard
     * c++ pointer to the data type specified
     * \return a pointer to the internal data
     */
    float * getRawDevPtr(){
        return thrust::raw_pointer_cast(&dDat[0]);
    }



    unsigned int rows;
    unsigned int cols;
    thrust::device_vector<float> dDat;  // this should probably be a shared pointer
    thrust::host_vector<float> hDat;    // this should probably be a shared pointer
};

__global__ void gpuKernel(float * x1,
                          float * x2,
                          float * outMat, unsigned int outMat_rows, unsigned int outMat_cols){

    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;
    int outIdx = j*outMat_rows+i;

    //printf("%d,%d : %d \n",i,j,outIdx);

    if(i<outMat_rows && j<outMat_cols){
        outMat[outIdx] = exp(-1*(abs(x1[i]-x2[j])));
        //outMat[outIdx] = x1[i]+x2[j];

        //printf("%d,%d : %d, %f \n",i,j,outIdx, outMat[outIdx]);
    }

}

cudaMat genKernel(cudaMat & x1, cudaMat & x2){

    cudaMat out;
    out.rows=x1.cols*x1.rows;
    out.cols=x2.cols*x2.rows;
    out.dDat=thrust::device_vector<float>(out.rows*out.cols,0);

    dim3 threads_per_block (16, 16, 1); // A 16 x 16 block threads
    dim3 number_of_blocks ((out.rows / threads_per_block.x) + 1, (out.cols / threads_per_block.y) + 1, 1);

    gpuKernel <<< number_of_blocks, threads_per_block >>> (x1.getRawDevPtr(),
                                           x2.getRawDevPtr(),
                                           out.getRawDevPtr(), out.rows , out.cols);

    cudacall(cudaGetLastError() );
    cudacall(cudaDeviceSynchronize() );

    return out;
}

void invert(cudaMat & in, cudaMat & out){
    int batchSize=1;
    cublasHandle_t handle;
    cublascall(cublasCreate_v2(&handle));

    int *P, *INFO;

    int lda = in.rows;
    int n = in.rows;

    cudacall(cudaMalloc(&P, n * batchSize * sizeof(int)));
    cudacall(cudaMalloc(&INFO,  batchSize * sizeof(int)));


    float **A=(float **)malloc(batchSize*sizeof(float *));
    float **A_d;
    cudacall(cudaMalloc(&A_d,batchSize*sizeof(float *)));
    A[0]=in.getRawDevPtr();

    cudacall(cudaMemcpy(A_d,A,batchSize*sizeof(float *),cudaMemcpyHostToDevice));

    cublascall(cublasSgetrfBatched(handle,n,A_d,lda,P,INFO,batchSize));


    int INFOh[batchSize];
    cudacall(cudaMemcpy(INFOh,INFO,batchSize*sizeof(int),cudaMemcpyDeviceToHost));

    for (int i = 0; i < batchSize; i++)
      if(INFOh[i]  != 0)
      {
        fprintf(stderr, "Factorization of matrix %d Failed: Matrix may be singular\n", i);
        cudaDeviceReset();
        exit(EXIT_FAILURE);
      }


    float **C = (float **)malloc(batchSize*sizeof(float *));
    float **C_d;
    cudacall(cudaMalloc(&C_d,batchSize*sizeof(float *)));

    C[0] = out.getRawDevPtr();
    for (int i = 1; i < batchSize; i++)
      C[i] = C[i-1] + (n*n);

    cudacall(cudaMemcpy(C_d,C,batchSize*sizeof(float *),cudaMemcpyHostToDevice));

    cublascall(cublasSgetriBatched(handle,n,(const float **)A_d,lda,P,C_d,lda,INFO,batchSize));

    cudacall(cudaMemcpy(INFOh,INFO,batchSize*sizeof(int),cudaMemcpyDeviceToHost));

    for (int i = 0; i < batchSize; i++)
      if(INFOh[i] != 0)
      {
        fprintf(stderr, "Inversion of matrix %d Failed: Matrix may be singular\n", i);
        cudaDeviceReset();
        exit(EXIT_FAILURE);
      }

    cudaFree(A_d); free(A);
    cudaFree(C_d); free(C);
    cudaFree(P); cudaFree(INFO); cublasDestroy_v2(handle);

}
    /*
    unsigned int n = in.cols;
    int lda = in.cols;
    cublasHandle_t handle;  // declare handle
    cublascall(cublasCreate_v2(&handle));   //initialize cublas
    int batchSize = 1;

    int *P, *INFO;
    cudacall(cudaMalloc<int>(&P,n * batchSize * sizeof(int)));  // create pivot array
    cudacall(cudaMalloc<int>(&INFO,batchSize * sizeof(int)));

    float *A[] = { in.getRawDevPtr() };  // create array of device matricies
    float** A_d;
    cudacall(cudaMalloc<float*>(&A_d,sizeof(A)));
    cudacall(cudaMemcpy(A_d,A,sizeof(A),cudaMemcpyHostToDevice));



    cublascall(cublasSgetrfBatched(handle,
                                   n,
                                   A_d,
                                   lda,
                                   P,
                                   INFO,
                                   batchSize));

    int INFOh = 0;
    cudacall(cudaMemcpy(&INFOh,INFO,sizeof(int),cudaMemcpyDeviceToHost));
    if(INFOh == n)
    {
        fprintf(stderr, "Factorization Failed: Matrix is singular\n");
        cudaDeviceReset();
        exit(EXIT_FAILURE);
    }


    out.dDat.resize(in.rows*in.cols);
    out.rows=in.rows;
    out.cols=in.cols;

    float* C[] = { out.getRawDevPtr() };
    float** C_d;
    cudacall(cudaMalloc<float*>(&C_d,sizeof(C)));
    cudacall(cudaMemcpy(C_d,C,sizeof(C),cudaMemcpyHostToDevice));
    cublascall(cublasSgetriBatched(handle,n,(const float **) A_d,lda,P,C_d,n,INFO,batchSize) );
    //cublasSgetriBatched(handle,n,A_d,lda,P,C_d,lda,INFO,batchSize)

    cudacall(cudaMemcpy(&INFOh,INFO,sizeof(int),cudaMemcpyDeviceToHost));

    if(INFOh != 0)
    {
        fprintf(stderr, "Inversion Failed: Matrix is singular\n");
        cudaDeviceReset();
        exit(EXIT_FAILURE);
    }

    cudaFree(P);
    cudaFree(INFO);
    cublasDestroy_v2(handle);


}
*/

cudaMat mult(cudaMat & a, cudaMat & b ,
             cublasOperation_t opA = CUBLAS_OP_N, cublasOperation_t opB = CUBLAS_OP_N,
             float alpha = 1.0f, float beta = 0.0f){

    cublasHandle_t handle;
    cublascall(cublasCreate_v2(&handle));

    int m = 0;
    if(opA==CUBLAS_OP_N)
        m=a.rows;
    else
        m=a.cols;

    int n = 0;
    if(opB==CUBLAS_OP_N)
        n=b.cols;
    else
        n=b.rows;

    int k = 0;
    if(opA==CUBLAS_OP_N)
        k=a.cols;
    else
        k=a.rows;


    cudaMat c;
    c.dDat=thrust::device_vector<float>(m*n,0);
    c.rows=m;
    c.cols=n;

    cublascall(cublasSgemm(handle,opA,opB,
                           m, n, k,
                           &alpha, a.getRawDevPtr(), a.ld(),
                                   b.getRawDevPtr(), b.ld(),
                           &beta,  c.getRawDevPtr(), c.ld()));

    cublasDestroy_v2(handle);
//    std::cout<< "c: " <<std::endl;Transpose[
//    c.print();
    return c;
}

/*!
 * \brief computes: y=y+alpha*x
 * \param y input/output: the cudaMat you want to add (alpha * x) to
 * \param x input only:  the cudaMat to add to y
 * \param alpha a constant scalar to multiply to x
 * \return
 */
void add(cudaMat & y, cudaMat & x, float alpha=1, int incy = 1, int incx = 1){
    cublasHandle_t handle;
    cublascall(cublasCreate_v2(&handle));
    cublascall(cublasSaxpy(handle,y.size(),&alpha,x.getRawDevPtr(),incx,y.getRawDevPtr(),incy));
    cublasDestroy_v2(handle);
}

void gpSolve(cudaMat & xTrain,
             cudaMat & yTrain,
             cudaMat & xPred,
             double sigma2e=0 ){

    cudaMat kx_x = genKernel(xTrain,xTrain);
    cudaMat kx_p = genKernel(xTrain,xPred);
    cudaMat kp_x = genKernel(xPred,xTrain);
    cudaMat var = genKernel(xPred,xPred);  // this is actually kp_p

    cudaMat vInv;
    vInv.hDat.resize(kx_x.hDat.size());
    vInv.dDat=thrust::device_vector<float>(kx_x.rows*kx_x.cols,0);
    vInv.rows=kx_x.cols;
    vInv.cols=kx_x.cols;
    invert(kx_x,vInv);

    cudaMat temp = mult( vInv , yTrain );

    cudaMat mu = mult( kp_x , temp );

    temp=mult(vInv,kx_p);
    temp=mult(kp_x,temp);
    add(var,temp,-1);

}



int main()
{
//    static const float arr[] = { 1,1.4,2.4,3,4.8,6, 8.1,9 };
//    std::vector<float> vec (arr, arr + sizeof(arr) / sizeof(arr[0]) );
    std::vector<float> vec;
    for(size_t i = 0 ; i<1000 ; i++)
        vec.push_back(std::rand()/((RAND_MAX)/10.0));
    cudaMat x;
    x.dDat=thrust::device_vector<float>(vec);
    x.cols=vec.size();
    x.rows=1;

    cudaMat pred;
    for(float i=0; i<=10 ; i=i+0.01){
        pred.hDat.push_back(i);
    }

    pred.dDat=pred.hDat;


    pred.rows=pred.hDat.size();;
    pred.cols=1;

    cudaMat y;
    y.cols=1;
    y.rows=vec.size();
    y.hDat=thrust::device_vector<float>(vec);
    for(size_t i = 0; i<vec.size(); i++){
        y.hDat[i] = sin( vec[i] );
    }

//    std::cout << "y: " << std::endl;
//    for(size_t i = 0 ; i< vec.size() ; i++){
//        std::cout <<  y.hDat[i] << ',';
//    }

    std::cout << std::endl << std::endl;

    y.dDat=y.hDat;



//    x.print();
//    y.print();
//    pred.print();


    gpSolve(x,y,pred);


    return 0;
}


/*

int main()
{


    static const float arr1[] = { 5,6,7,8};
    std::vector<float> vec1 (arr1, arr1 + sizeof(arr1) / sizeof(arr1[0]) );
    cudaMat x1;
    x1.dDat=thrust::device_vector<float>(vec1);
    x1.cols=2;
    x1.rows=2;

    static const float arr2[] = { 9,10,11,12};
    std::vector<float> vec2 (arr2, arr2 + sizeof(arr2) / sizeof(arr2[0]) );
    cudaMat x2;
    x2.dDat=thrust::device_vector<float>(vec2);
    x2.cols=2;
    x2.rows=2;

    std::cout<< std::endl << "x1: " << std::endl;
    x1.print();

    std::cout<< std::endl << "x2: " << std::endl;
    x2.print();

    add(x2,x1,-1.0);

    std::cout<< std::endl << "x2: " << std::endl;
    x2.print();


    return 0;
}
*/




//    static const int arr[] = { 0.5, 3, 4,
//                               1, 3, 10,
//                               4 , 9, 16 };
//    std::vector<int> vec (arr, arr + sizeof(arr) / sizeof(arr[0]) );
//    cudaMat x1;
//    x1.dDat=thrust::device_vector<float>(vec);
//    x1.cols=3;
//    x1.rows=3;
//    //x1.print();

//    cudaMat x2;
//    x2.dDat=thrust::device_vector<float>(3*3,0);
//    x2.cols=3;
//    x2.rows=3;

//    x1.print();
//    std::cout<<std::endl;
//    invert(x1,x2);
//    x1.print();
//    x2.print();


//    cudaMat x1;
//    x1.dDat=thrust::device_vector<float>(10000,1);
//    x1.cols=x1.dDat.size();
//    x1.rows=1;

//    cudaMat x2;
//    x2.dDat=thrust::device_vector<float>(10000,2);
//    x2.cols=x2.dDat.s)ize();
//    x2.rows=1;

//    cudaMat out;
//    out.dDat=thrust::device_vector<float>(x1.cols*x2.cols,0);
//    out.rows=x1.cols;
//    out.cols=x2.cols;

//     cudaError_t syncErr, asyncErr;


//    dim3 threads_per_block (16, 16, 1); // A 16 x 16 block threads
//    dim3 number_of_blocks ((out.rows / threads_per_block.x) + 1, (out.cols / threads_per_block.y) + 1, 1);
////    dim3 threads_per_block (10, 10, 1); // A 16 x 16 block threads
//    //out.print();
//    std::cout << std::endl;

//    gpuKernel <<< number_of_blocks, threads_per_block >>> (x1.getRawDevPtr(),
//                                           x2.getRawDevPtr(),
//                                           out.getRawDevPtr(), out.rows , out.cols);

//    syncErr = cudaGetLastError();
//    asyncErr = cudaDeviceSynchronize();

    //out.print();
